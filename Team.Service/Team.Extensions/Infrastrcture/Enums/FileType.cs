﻿namespace Team.Extensions.Infrastrcture.Enums
{
    public enum FileType
    {
        Unknown,
        Photo,
        Video,
        Audio,
        Document,
        Archive,
        Executable
    }
}