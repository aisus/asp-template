﻿namespace Team.Extensions.Infrastrcture.Enums
{
    public enum MessageStatus
    {
        Info,
        Error,
        Warning,
        Success
    }
}