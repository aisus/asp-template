using System;
using System.Collections.Generic;
using Team.Extensions.Infrastrcture;

namespace Team.DAL.Models
{
    public class Comment : EntityWithCreatedDate
    {
        public string Content { get; set; }
        public Guid AuthorUserId { get; set; }
        public Guid PostId { get; set; }
        public virtual User Author { get; set; }
        public virtual Post Post { get; set; }
        public virtual List<Like> Likes { get; set; }
    }
}