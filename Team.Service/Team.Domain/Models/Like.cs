using System;
using Team.Extensions.Infrastrcture;

namespace Team.DAL.Models
{
    public class Like : EntityWithCreatedDate
    {
        public Guid AuthorUserId { get; set; }
        public Guid? PostId {get; set; }
        public Guid? CommentId {get; set; }
        public virtual User Author { get; set; }
        public virtual Post Post { get; set; }
        public virtual Comment Comment { get; set; }
    }
}