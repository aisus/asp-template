using System;
using System.Collections.Generic;
using Team.Extensions.Infrastrcture;

namespace Team.DAL.Models
{
    public class Post : EntityWithCreatedDate
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public Guid AuthorUserId { get; set; }
        public User Author { get; set; }
        public List<Like> Likes { get; set; }
        public List<Comment> Comments { get; set; }
    }
}