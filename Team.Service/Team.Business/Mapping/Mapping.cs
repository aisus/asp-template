﻿using Mapster;
using Team.Business.DTO;
using Team.DAL.Models;

namespace Team.Business.Mapping
{
    public class Mapping : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<UserInputDto, User>().IgnoreNullValues(true);
            config.ForType<User, UserOutputDto>().IgnoreNullValues(true);

            config.ForType<PostInputDto, Post>().IgnoreNullValues(true);
            config.ForType<Post, PostOutputDto>().IgnoreNullValues(true);

            config.ForType<CommentInputDto, Comment>().IgnoreNullValues(true);
            config.ForType<Comment, CommentOutputDto>().IgnoreNullValues(true);

            config.ForType<LikeFullInputDto, Like>().IgnoreNullValues(true);
            config.ForType<Like, LikeOutputDto>().IgnoreNullValues(true);
        }
    }
}