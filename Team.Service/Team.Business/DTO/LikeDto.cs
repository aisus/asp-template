using System;

namespace Team.Business.DTO
{
    public class LikeInputDto
    {
        public Guid AuthorUserId { get; set; }
    }

    public class LikeFullInputDto
    {
        public Guid AuthorUserId { get; set; }
        public Guid? PostId {get; set; }
        public Guid? CommentId {get; set; }
    }

    public class LikeOutputDto
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid AuthorUserId { get; set; }
        public Guid? PostId {get; set; }
        public Guid? CommentId {get; set; }
    }
}