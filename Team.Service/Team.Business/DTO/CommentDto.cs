using System;
using System.Collections.Generic;

namespace Team.Business.DTO
{
    public class CommentInputDto
    {
        public string Content { get; set; }
        public Guid AuthorUserId { get; set; }
        public Guid PostId { get; set; }
    }

    public class CommentOutputDto
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid AuthorUserId { get; set; }
        public Guid PostId { get; set; }
        public List<LikeOutputDto> Likes { get; set; }
    }
}