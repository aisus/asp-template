using System;

namespace Team.Business.DTO
{
    public class UserInputDto
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
    }

    public class UserOutputDto
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public DateTime Birthday { get; set; }
    }
}