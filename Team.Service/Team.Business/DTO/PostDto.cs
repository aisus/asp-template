using System;
using System.Collections.Generic;

namespace Team.Business.DTO
{
    public class PostInputDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public Guid AuthorUserId { get; set; }
    }

    public class PostOutputDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid AuthorUserId { get; set; }
        public List<CommentOutputDto> Comments { get; set; }
        public List<LikeOutputDto> Likes { get; set; }
    }
}