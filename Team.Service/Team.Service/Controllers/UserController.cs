﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Team.Business.DTO;
using Team.DAL;
using Team.DAL.Models;
using Team.Extensions.DAL;
using Team.Extensions.Infrastrcture;
using Team.Extensions.Web;

namespace Team.Service.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : CustomApiController
    {
        private readonly IAsyncCrudService<User> _service;
        private readonly IAsyncOrderedQueryService<Post> _postsService;
        private readonly IAsyncOrderedQueryService<Comment> _commentService;
        private readonly IAsyncOrderedQueryService<Like> _likeService;
        private readonly DbContext _dbContext; 

        public UserController(IAsyncCrudService<User> service, IAsyncOrderedQueryService<Post> postsService, IAsyncOrderedQueryService<Comment> commentService, IAsyncOrderedQueryService<Like> likeService, DbContext dbContext)
        {
            _service = service;
            _postsService = postsService;
            _commentService = commentService;
            _likeService = likeService;
            _dbContext = dbContext;
        }

        [HttpGet("{id:guid}")]
        [ProducesResponseType(typeof(UserOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> GetByGuid(Guid id)
        {
            return ModelAsync(_service.GetRecordAsync<UserOutputDto>(id));
        }

        [HttpGet("page")]
        [ProducesResponseType(typeof(CollectionOutputModel<UserOutputDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> GetPage([Required][Range(0, 999)] int limit, int page = 0)
        {
            var pmodel = new PageModel(limit, page);
            return ModelAsync(_service.GetPageAsync<UserOutputDto>(pmodel, x => true));
        }

        [HttpPost()]
        [ProducesResponseType(typeof(UserOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Create(UserInputDto dto)
        {
            var check = await _dbContext.Set<User>().FirstOrDefaultAsync(x => x.UserName == dto.UserName);
            if (check != null)
            {
                return BadRequestResult("Username already exists");
            }

            return await ModelAsync(_service.CreateAsync(dto));
        }

        [HttpPut("{id:guid}")]
        [ProducesResponseType(typeof(UserOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> Update(Guid id, UserInputDto dto)
        {
            return ModelAsync(_service.UpdateAsync(id, dto));
        }

        [HttpDelete("{id:guid}")]
        [ProducesResponseType(typeof(UserOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> Delete(Guid id)
        {
            return ModelAsync(_service.DeleteAsync(id));
        }

        [HttpGet("{id:guid}/posts")]
        [ProducesResponseType(typeof(CollectionOutputModel<PostOutputDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> GetPostsPage(Guid id, [Required][Range(0, 999)] int limit, int page = 0)
        {
            var pmodel = new PageModel(limit, page);
            return ModelAsync(_postsService.GetPageAsync<PostOutputDto, DateTime?>(pmodel, x => x.AuthorUserId == id, x => x.CreatedDate, true));
        }

        [HttpGet("{id:guid}/comments")]
        [ProducesResponseType(typeof(CollectionOutputModel<CommentOutputDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> GetCommentsPage(Guid id, [Required][Range(0, 999)] int limit, int page = 0)
        {
            var pmodel = new PageModel(limit, page);
            return ModelAsync(_commentService.GetPageAsync<CommentOutputDto, DateTime?>(pmodel, x => x.AuthorUserId == id, x => x.CreatedDate, true));
        }

        [HttpGet("{id:guid}/likes")]
        [ProducesResponseType(typeof(CollectionOutputModel<LikeOutputDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> GetLikesPage(Guid id, [Required][Range(0, 999)] int limit, int page = 0)
        {
            var pmodel = new PageModel(limit, page);
            return ModelAsync(_likeService.GetPageAsync<LikeOutputDto, DateTime?>(pmodel, x => x.AuthorUserId == id, x => x.CreatedDate, true));
        }
    }
}