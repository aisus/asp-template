﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Team.Business.DTO;
using Team.DAL.Models;
using Team.Extensions.DAL;
using Team.Extensions.Infrastrcture;
using Team.Extensions.Web;
using Mapster;
using Team.DAL;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Team.Service.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PostController : CustomApiController
    {
        private readonly IAsyncCrudService<Post> _service;
        private readonly IAsyncCrudService<Like> _likeService;
        private readonly IAsyncOrderedQueryService<Post> _orderedQueryService;
        private readonly DbContext _dbContext; 

        public PostController(IAsyncCrudService<Post> service, IAsyncCrudService<Like> likeService, IAsyncOrderedQueryService<Post> orderedQueryService, DbContext dbContext)
        {
            _service = service;
            _likeService = likeService;
            _dbContext = dbContext;
            _orderedQueryService = orderedQueryService;
        }

        [HttpGet("{id:guid}")]
        [ProducesResponseType(typeof(PostOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> GetByGuid(Guid id)
        {
            return ModelAsync(_service.GetRecordAsync<PostOutputDto>(id));
        }

        [HttpGet("page")]
        [ProducesResponseType(typeof(CollectionOutputModel<PostOutputDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> GetPage([Required][Range(0, 999)]int limit, int page = 0)
        {
            var pmodel = new PageModel(limit, page);
            return ModelAsync(_orderedQueryService.GetPageAsync<PostOutputDto, DateTime?>(pmodel, x => true, x => x.CreatedDate, true));
        }

        [HttpPost()]
        [ProducesResponseType(typeof(PostOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> Create(PostInputDto dto)
        {
            return ModelAsync(_service.CreateAsync(dto));
        }

        [HttpPut("{id:guid}")]
        [ProducesResponseType(typeof(PostOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> Update(Guid id, PostInputDto dto)
        {
            return ModelAsync(_service.UpdateAsync(id, dto));
        }

        [HttpDelete("{id:guid}")]
        [ProducesResponseType(typeof(PostOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> Delete(Guid id)
        {
            return ModelAsync(_service.DeleteAsync(id));
        }

        [HttpPost("{id:guid}/like")]
        [ProducesResponseType(typeof(LikeOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Like(Guid id, LikeInputDto dto)
        {
            var check = await _dbContext
            .Set<Like>()
            .FirstOrDefaultAsync(x => x.AuthorUserId == dto.AuthorUserId && x.PostId == id);

            if (check != null)
            {
                return BadRequestResult("User already liked this post");
            }

            var fullDto = dto.Adapt<LikeFullInputDto>();
            fullDto.PostId = id;

            return await ModelAsync(_likeService.CreateAsync(fullDto));
        }

        [HttpDelete("{id:guid}/like")]
        [ProducesResponseType(typeof(LikeOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Unlike(Guid id, LikeInputDto dto)
        {
            var check = await _dbContext
            .Set<Like>()
            .FirstOrDefaultAsync(x => x.AuthorUserId == dto.AuthorUserId && x.PostId == id);

            if (check == null)
            {
                return BadRequestResult("This post has no likes from user");
            }

            return await ModelAsync(_likeService.DeleteAsync(check.Id));
        }
    }
}