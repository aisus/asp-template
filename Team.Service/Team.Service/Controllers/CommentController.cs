﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Team.Business.DTO;
using Team.DAL.Models;
using Team.Extensions.DAL;
using Team.Extensions.Infrastrcture;
using Team.Extensions.Web;

namespace Team.Service.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CommentController : CustomApiController
    {
        private readonly IAsyncCrudService<Comment> _service;
        private readonly IAsyncCrudService<Like> _likeService;
        private readonly DbContext _dbContext; 

        public CommentController(IAsyncCrudService<Comment> service, IAsyncCrudService<Like> likeService, DbContext dbContext)
        {
            _service = service;
            _likeService = likeService;
            _dbContext = dbContext;
        }

        [HttpPost()]
        [ProducesResponseType(typeof(CommentOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> Create(CommentInputDto dto)
        {
            return ModelAsync(_service.CreateAsync(dto));
        }

        [HttpPut("{id:guid}")]
        [ProducesResponseType(typeof(CommentOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> Update(Guid id, CommentInputDto dto)
        {
            return ModelAsync(_service.UpdateAsync(id, dto));
        }

        [HttpDelete("{id:guid}")]
        [ProducesResponseType(typeof(CommentOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public Task<IActionResult> Delete(Guid id)
        {
            return ModelAsync(_service.DeleteAsync(id));
        }

        [HttpPost("{id:guid}/like")]
        [ProducesResponseType(typeof(LikeOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Like(Guid id, LikeInputDto dto)
        {
             var check = await _dbContext
            .Set<Like>()
            .FirstOrDefaultAsync(x => x.AuthorUserId == dto.AuthorUserId && x.CommentId == id);

            if (check != null)
            {
                return BadRequestResult("User already liked this comment");
            }

            var fullDto = dto.Adapt<LikeFullInputDto>();
            fullDto.CommentId = id;

            return await ModelAsync(_likeService.CreateAsync(fullDto));
        }

        [HttpDelete("{id:guid}/like")]
        [ProducesResponseType(typeof(LikeOutputDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(MessageResult), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Unlike(Guid id, LikeInputDto dto)
        {
            var check = await _dbContext
            .Set<Like>()
            .FirstOrDefaultAsync(x => x.AuthorUserId == dto.AuthorUserId && x.CommentId == id);

            if (check == null)
            {
                return BadRequestResult("This comment has no likes from user");
            }

            return await ModelAsync(_likeService.DeleteAsync(check.Id));
        }
    }
}